import Vue from 'vue'
import App from './App.vue'
import store from './store'
import VueRamda from 'vue-ramda'


import './../node_modules/bulma/css/bulma.css'
import './css/main.css'

import { library } from '@fortawesome/fontawesome-svg-core'
import { faHeart, faFistRaised, faShoePrints, faCheck, faTimes } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(faHeart)
library.add(faFistRaised)
library.add(faShoePrints)
library.add(faCheck)
library.add(faTimes)

Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.config.productionTip = false
Vue.use(VueRamda)

new Vue({
  store,
  render: h => h(App)
}).$mount('#app')
